import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve, reject) => {
    // resolve the promise with the winner when fight is over


    const healthBarLeft = document.getElementById('left-fighter-indicator');
    const healthBarRight = document.getElementById('right-fighter-indicator');

    healthBarLeft.style.width = '100%';
    healthBarRight.style.width = '100%';

    const state = {
      playerOne: { lastCritTime: null, isBlocking: false, health: firstFighter.health },
      playerTwo: { lastCritTime: null, isBlocking: false, health: secondFighter.health },
    };

    document.addEventListener('keyup', event => {
      const key = event.code;
      switch (key) {
        case controls.PlayerOneAttack:
          getHitPower(firstFighter);
          break;
        case controls.PlayerTwoAttack:
          getHitPower(secondFighter);
          break;
        case controls.PlayerOneBlock:
          getBlockPower(firstFighter);
          break;
        case controls.PlayerTwoBlock:
          getBlockPower(secondFighter);
          break;
        default:
          break;
      }
    })
    if (state.playerOne.health <= 0) {
      resolve(secondFighter);
    } else if (state.playerTwo.health <= 0) {
      resolve(firstFighter);
    }
  });
}

const canCrit = lastCritTime => lastCritTime ? Math.floor((new Date() - lastCritTime) / 1000) > 10 : true;

const getCritDamage = fighter => 2 * fighter.attack;

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * getRandomNumber();
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * getRandomNumber();
}

const getRandomNumber = () => Math.random() * (2 - 1) + 1;